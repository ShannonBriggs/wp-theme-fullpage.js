<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php include 'blocks/head.php'; ?>
<body <?php body_class(); ?>>
<!--<div id="wrapper" class="hfeed">-->
<!--    <header id="header" role="banner">-->
<!--        <section id="branding">-->
<!--            <div id="site-title">--><?php //if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '<h1>'; } ?><!--<a href="--><?php //echo esc_url( home_url( '/' ) ); ?><!--" title="--><?php //echo esc_html( get_bloginfo( 'name' ) ); ?><!--" rel="home">--><?php //echo esc_html( get_bloginfo( 'name' ) ); ?><!--</a>--><?php //if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '</h1>'; } ?><!--</div>-->
<!--            <div id="site-description">--><?php //bloginfo( 'description' ); ?><!--</div>-->
<!--        </section>-->
<!--        <nav id="menu" role="navigation">-->
<!--            <div id="search">-->
<!--                --><?php //get_search_form(); ?>
<!--            </div>-->
<!--            --><?php //wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
<!--        </nav>-->
<!--    </header>-->
<!--    <div id="container">-->




<?php include 'blocks/nav.php'; ?>


<div id="fullpage">
    <div class="section" id="section0">
        <?php include 'sections/section0.php'; ?>
    </div>
    <div class="section" id="section1">
        <?php include 'sections/section1.php'; ?>
    </div>
    <div class="section" id="section2">
        <?php include 'sections/section2.php'; ?>
    </div>
    <div class="section" id="section3">
        <?php include 'sections/section3.php'; ?>
    </div>
    <div class="section" id="section4">
        <?php include 'sections/section4.php'; ?>
    </div>
    <div class="section fp-auto-height" id="section5">
        <?php include 'sections/section5.php'; ?>
    </div>
</div>