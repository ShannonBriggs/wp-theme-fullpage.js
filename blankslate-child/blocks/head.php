<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo Get_template_directory_uri() ?>-child/css/responsive.css" />
    <?php wp_head(); ?>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



    <link rel="stylesheet" type="text/css" href="<?php echo Get_template_directory_uri() ?>-child/css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Get_template_directory_uri() ?>-child/css/examples.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Get_template_directory_uri() ?>-child/css/sections.css" />

    <!--[if IE]>
    <script type="text/javascript">
        var console = { log: function() {} };
    </script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <script src="<?php echo Get_template_directory_uri() ?>-child/js/jquery.easings.min.js"></script>
    <script type="text/javascript" src="<?php echo Get_template_directory_uri() ?>-child/js/scrolloverflow.min.js"></script>


    <script type="text/javascript" src="<?php echo Get_template_directory_uri() ?>-child/js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="<?php echo Get_template_directory_uri() ?>-child/js/settings.js"></script>


</head>





