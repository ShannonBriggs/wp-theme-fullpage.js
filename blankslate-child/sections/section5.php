
    <div class="myContent">
        <div class="intro">
            <h1>Footer</h1>
            <p style="text-align: center">This demonstrates that not every section needs to be full screen.</p>
            <p style="text-align: center">A website footer might only need to display a small amount of
                information</p>
        </div>
    </div>
